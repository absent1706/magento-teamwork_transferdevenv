# Useful development helpers for Teamwork Transfer module #
This "Development environment" rewrites Teamwork Transfer module to make its development more easy.

# Features #
By setting options in *etc/config.xml*, it allows to:

 * set, whether to run reindex after ECM processing (*do_reindex* option)
 * set, whether to clean cache after ECM processing (*clean_cache* option)
 * skip running Teamwork_Transfer_Model_Transfer::executeBlock() method while processing ECM (by enabling *execute_blocks* option), which is useful if you want to stub ECM processing