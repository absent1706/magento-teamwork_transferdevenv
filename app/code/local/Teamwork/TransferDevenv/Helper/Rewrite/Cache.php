<?php

class Teamwork_TransferDevenv_Helper_Rewrite_Cache extends Teamwork_Transfer_Helper_Cache
{
    public function cleanCache($ecm)
    {
        return (Mage::getStoreConfigFlag(Teamwork_TransferDevenv_Helper_Config::XML_PATH_CLEAN_CACHE)) ? parent::cleanCache($ecm) : true;
    }
}