<?php

class Teamwork_TransferDevenv_Helper_Rewrite_Reindex extends Teamwork_Transfer_Helper_Reindex
{
    public function runIndexer($ecm)
    {
        return (Mage::getStoreConfigFlag(Teamwork_TransferDevenv_Helper_Config::XML_PATH_DO_REINDEX)) ? parent::runIndexer($ecm) : true;
    }
}