<?php
class Teamwork_TransferDevenv_Helper_Config extends Teamwork_Transfer_Helper_Data
{
    const XML_PATH_DO_REINDEX        = 'teamwork_transferdevenv/general/do_reindex';
    const XML_PATH_CLEAN_CACHE    = 'teamwork_transferdevenv/general/clean_cache';

    const XML_PATH_EXECUTE_BLOCKS = 'teamwork_transferdevenv/general/execute_blocks';
}