<?php
class Teamwork_TransferDevenv_Model_Rewrite_Transfer extends Teamwork_Transfer_Model_Transfer
{
    protected function executeBlock($type = '')
    {
        return (Mage::getStoreConfigFlag(Teamwork_TransferDevenv_Helper_Config::XML_PATH_EXECUTE_BLOCKS)) ? parent::executeBlock($type) : true;
    }
}